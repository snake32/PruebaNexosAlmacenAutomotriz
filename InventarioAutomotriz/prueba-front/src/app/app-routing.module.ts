import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ProductoFormEditComponent } from './pages/producto/producto-form-edit/producto-form-edit.component';
import { ProductoFormComponent } from './pages/producto/producto-form/producto-form.component';
import { ProductoListComponent } from './pages/producto/producto-list/producto-list.component';
import { UsuarioAsignarComponent } from './pages/usuario/usuario-asignar/usuario-asignar.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'nexos',
    pathMatch:'full'
  },
  {
    path : 'nexos',
    component: UsuarioAsignarComponent
  },
  {
    path : 'productos',
    component: ProductoListComponent
  },
  {
    path : 'productos/nuevo',
    component : ProductoFormComponent
  },
  {
    path : 'productos/editar/:id',
    component : ProductoFormEditComponent
  },
];


@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
