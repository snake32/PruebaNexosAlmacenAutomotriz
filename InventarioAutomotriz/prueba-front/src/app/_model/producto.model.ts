import { Usuario } from "./usuario.model";

export class Producto {

    idProducto ?: number;
    usuario ?: Usuario;
    nombre ?: string;
    cantidad ?: number;
    fechaIngreso ?: Date
    usuarioModificacion ?: string;

}