import { Cargo } from "./cargo.model";

 export class Usuario {

    idUsuario ?: number;
    cargo ?: Cargo;
    nombre ?: string;
    edad ?: number;
    fechaIngresoCompañia ?: Date

}