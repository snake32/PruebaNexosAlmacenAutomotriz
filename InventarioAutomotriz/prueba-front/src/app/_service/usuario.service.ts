import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Usuario } from '../_model/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class UsuarioService {

  private url = "http://localhost:8080/usuarios";

  constructor(private http: HttpClient) { }

  findAll(): Observable<Usuario[]>{
    return this.http.get<Usuario[]>(`${this.url}`);
  }

}