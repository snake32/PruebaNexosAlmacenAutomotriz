import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { Producto } from '../_model/producto.model';
import { Usuario } from '../_model/usuario.model';

@Injectable({
  providedIn: 'root'
})
export class ProductoService {

  private url = "http://localhost:8080/productos";

  constructor(private http: HttpClient) { }

  usuario ?: Usuario;

  findAll(): Observable<Producto[]>{
    return this.http.get<Producto[]>(this.url);
  }

  findByFiltro(idUsuario: number, nombre: string): Observable<Producto[]>{
    return this.http.get<Producto[]>( this.url + "/filtro" + "?usuario=" + idUsuario+ "&nombre=" + nombre );
  }

  findById( idProducto: number,): Observable<Producto>{
    return this.http.get<Producto>( this.url + "/" + idProducto );
  }

  save( producto : Producto ){
    return this.http.post<Producto>( this.url , producto );
  }

  update( producto : Producto, idProducto: number ){
    return this.http.put<Producto>( this.url + "/" + idProducto , producto );
  }

  delete( idProducto: number, idUsuario: number ){
    return this.http.delete(this.url + "/" + idProducto + "?idUsuario="+idUsuario);
  }

}
