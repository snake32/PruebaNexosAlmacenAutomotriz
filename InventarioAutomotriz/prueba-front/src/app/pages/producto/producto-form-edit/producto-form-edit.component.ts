import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { ActivatedRoute, Router } from '@angular/router';
import { fadeInUp400ms } from 'src/app/animations/fade-in-up.animation';
import { Producto } from 'src/app/_model/producto.model';
import { ProductoService } from 'src/app/_service/producto.service';

@Component({
  selector: 'app-producto-form-edit',
  templateUrl: './producto-form-edit.component.html',
  styleUrls: ['./producto-form-edit.component.scss'],
  animations: [
    fadeInUp400ms
  ]
})
export class ProductoFormEditComponent implements OnInit {

  form: FormGroup;
  id : number;
  producto ?: Producto;

  constructor(private route: ActivatedRoute,
    public router: Router,
    private productoService: ProductoService,
    private snackBar: MatSnackBar) {

      let varId = route.snapshot.paramMap.get('id');
      this.id = Number(varId);

      this.form = new FormGroup({
        'id' : new FormControl(0),
        'nombre' : new FormControl(''),
        'cantidad' : new FormControl(''),
        'fechaIngreso' : new FormControl('')
      });

   }

  ngOnInit(): void {
    this.getUsuario();
    this.getProduct();
  }

  getUsuario(){
    let usuarioFound = this.productoService.usuario;

    if(usuarioFound === undefined || usuarioFound === null){
      this.router.navigate(['nexos']);
    }

  }

  getProduct() {
    this.productoService.findById(this.id).subscribe((data) => {
      this.producto = data;
      this.form.controls['id'].setValue(this.producto.idProducto);
      this.form.controls['nombre'].setValue(this.producto.nombre);
      this.form.controls['cantidad'].setValue(this.producto.cantidad);
      this.form.controls['fechaIngreso'].setValue(this.producto.fechaIngreso);

    },
    (error) => {
      this.snackBar.open(error.error.message, "Mensaje de error", {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "center"
      });
    });
  }

  guardar(){
    this.producto = new Producto;
    this.producto.nombre = this.form.value['nombre'];
    this.producto.cantidad = this.form.value['cantidad'];
    this.producto.fechaIngreso = this.form.value['fechaIngreso'];
    this.producto.usuarioModificacion = this.productoService.usuario?.nombre;

    this.productoService.update(this.producto, this.id).subscribe((data) => {
      this.router.navigate(['productos']);
      this.snackBar.open("El producto se actualizo exitosamente", "Mensaje de éxito", {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "center"
      });
    },
    (error) => {
      this.snackBar.open(error.error.message, "Mensaje de error", {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "center"
      });
    });

  }

}