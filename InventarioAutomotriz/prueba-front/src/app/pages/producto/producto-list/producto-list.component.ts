import { Component, OnInit, ViewChild } from '@angular/core';
import { Producto } from 'src/app/_model/producto.model';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { ProductoService } from 'src/app/_service/producto.service';
import { ProductoEliminarComponent } from '../producto-eliminar/producto-eliminar.component';
import { MatDialog } from '@angular/material/dialog';
import { Usuario } from 'src/app/_model/usuario.model';
import { Cargo } from 'src/app/_model/cargo.model';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-producto-list',
  templateUrl: './producto-list.component.html',
  styleUrls: ['./producto-list.component.scss']
})
export class ProductoListComponent implements OnInit {

  displayedColumns: string[] = ['idProducto', 'nombre', 'cantidad', 'fechaIngreso', 'usuario', 'acciones'];
  dataSource = new MatTableDataSource<Producto>;

  usuarioBuscar?: number;
  nombreBuscar: string;

  @ViewChild(MatPaginator) paginator!: MatPaginator;
  @ViewChild(MatSort) sort!: MatSort;
  pagesize = 10;

  usuario?: Usuario;
  usuarios?:Usuario[];

  constructor(private productoService : ProductoService,
    private usuarioService : UsuarioService,
    public dialog: MatDialog,
    public router: Router) {
      this.nombreBuscar = "";
  }

  ngOnInit(): void {
    this.getUsuario();
    this.buscar();
    this.getUsuarios();
  }

  getUsuarios(){
    this.usuarioService.findAll().subscribe(data => {
      this.usuarios = data;
    });
  }

  getUsuario(){
    let usuarioFound = this.productoService.usuario;

    if(usuarioFound === undefined || usuarioFound === null){
      this.router.navigate(['nexos']);
    }

  }

  getProductos(){
    this.productoService.findAll().subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

  eliminar(id : any, nombre : string){
    const dialogRef = this.dialog.open(ProductoEliminarComponent,{
      data:{
        nombre : nombre,
        id: id,
        idUsuario : this.usuario?.idUsuario
      }
    });

    dialogRef.afterClosed().subscribe(result => {
      this.getProductos();
    });
  }

  buscar(){
    let usuarioB = 0;
    if(this.usuarioBuscar !== undefined){
      usuarioB = this.usuarioBuscar;
    }

    this.productoService.findByFiltro(usuarioB, this.nombreBuscar).subscribe(data => {
      this.dataSource = new MatTableDataSource(data);
      this.dataSource.paginator = this.paginator;
      this.dataSource.sort = this.sort;
    });
  }

}