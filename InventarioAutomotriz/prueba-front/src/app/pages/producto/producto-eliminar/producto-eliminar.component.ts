import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { ProductoService } from 'src/app/_service/producto.service';

@Component({
  selector: 'app-producto-eliminar',
  templateUrl: './producto-eliminar.component.html',
  styleUrls: ['./producto-eliminar.component.scss']
})
export class ProductoEliminarComponent implements OnInit {

  title = 'Eliminar producto';
  nombre : string ='';
  id : number =0;
  idUsuario : number =0;
  constructor(
    @Inject(MAT_DIALOG_DATA) private data: any,
    public dialogRef: MatDialogRef<ProductoEliminarComponent>,
    private productoService : ProductoService,
    public router: Router,
    private snackBar: MatSnackBar) {
      this.nombre = data.nombre || this.nombre;
      this.id = data.id || this.id;
      this.idUsuario = data.idUsuario || this.idUsuario;
  }

  ngOnInit(): void {
  }
  onNoClick(): void {
    this.dialogRef.close();
  }
  eliminar(){

    this.productoService.delete(this.id, this.idUsuario).subscribe((data) => {
      this.snackBar.open("El producto se elimino exitosamente", "Mensaje de éxito", {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "center"
      });
      this.dialogRef.close();
    },
    (error) => {
      this.snackBar.open(error.error.message, "Mensaje de error", {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "center"
      });
    });

  }

}