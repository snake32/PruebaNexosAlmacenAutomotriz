import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { Router } from '@angular/router';
import { Producto } from 'src/app/_model/producto.model';
import { Usuario } from 'src/app/_model/usuario.model';
import { ProductoService } from 'src/app/_service/producto.service';
import { UsuarioService } from 'src/app/_service/usuario.service';
import { fadeInUp400ms } from '../../../animations/fade-in-up.animation';

@Component({
  selector: 'app-producto-form',
  templateUrl: './producto-form.component.html',
  styleUrls: ['./producto-form.component.scss'],
  animations: [
    fadeInUp400ms
  ]
})
export class ProductoFormComponent implements OnInit {

  form: FormGroup;
  producto?: Producto;

  constructor(private productoService : ProductoService,
    private usuarioService : UsuarioService,
    public router: Router,
    private snackBar: MatSnackBar){
    this.form = new FormGroup({
      'id' : new FormControl(0),
      'nombre' : new FormControl(''),
      'cantidad' : new FormControl(''),
      'fechaIngreso' : new FormControl('')
    });
  }

  ngOnInit(): void {
    this.getUsuario();
  }

  getUsuario(){
    let usuarioFound = this.productoService.usuario;

    if(usuarioFound === undefined || usuarioFound === null){
      this.router.navigate(['nexos']);
    }

  }

  guardar(){

    this.producto = new Producto;
    this.producto.nombre = this.form.value['nombre'];
    this.producto.cantidad = this.form.value['cantidad'];
    this.producto.fechaIngreso = this.form.value['fechaIngreso'];
    this.producto.usuario = this.productoService.usuario;

    this.productoService.save(this.producto).subscribe((data) => {
      this.router.navigate(['productos']);
      this.snackBar.open("El producto se registro exitosamente", "Mensaje de éxito", {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "center"
      });
    },
    (error) => {
      this.snackBar.open(error.error.message, "Mensaje de error", {
        duration: 2000,
        verticalPosition: "top",
        horizontalPosition: "center"
      });
    });

  }

}