import { ComponentFixture, TestBed } from '@angular/core/testing';

import { UsuarioAsignarComponent } from './usuario-asignar.component';

describe('UsuarioAsignarComponent', () => {
  let component: UsuarioAsignarComponent;
  let fixture: ComponentFixture<UsuarioAsignarComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ UsuarioAsignarComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(UsuarioAsignarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
