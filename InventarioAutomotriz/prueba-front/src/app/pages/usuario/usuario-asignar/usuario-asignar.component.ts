import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Usuario } from 'src/app/_model/usuario.model';
import { ProductoService } from 'src/app/_service/producto.service';
import { UsuarioService } from 'src/app/_service/usuario.service';

@Component({
  selector: 'app-usuario-asignar',
  templateUrl: './usuario-asignar.component.html',
  styleUrls: ['./usuario-asignar.component.scss']
})
export class UsuarioAsignarComponent implements OnInit {

  usuarioBuscar?: Usuario;
  usuarios?:Usuario[];

  constructor(private usuarioService : UsuarioService,
    public router: Router,
    private productoService : ProductoService) { }

  ngOnInit(): void {
    this.getUsuarios();
  }

  getUsuarios(){
    this.usuarioService.findAll().subscribe(data => {
      this.usuarios = data;
    });
  }

  asignar(){
    this.productoService.usuario = this.usuarioBuscar;
    this.router.navigate(['productos']);
  }

}
