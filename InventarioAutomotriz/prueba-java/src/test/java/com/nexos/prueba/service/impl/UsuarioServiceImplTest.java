package com.nexos.prueba.service.impl;

import com.nexos.prueba.entity.UsuarioEntity;
import com.nexos.prueba.mapper.UsuarioMapper;
import com.nexos.prueba.repository.UsuarioRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.when;

class UsuarioServiceImplTest {

    @Mock
    private UsuarioRepository usuarioRepository;

    @Mock
    private UsuarioMapper usuarioMapper;

    @InjectMocks
    private UsuarioServiceImpl usuarioServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll() {
        when(usuarioRepository.findAll()).thenReturn(getUsuarios());
        assertNotNull(usuarioServiceImpl.findAll());
    }

    List<UsuarioEntity> getUsuarios(){
        return new ArrayList<>();
    }

}