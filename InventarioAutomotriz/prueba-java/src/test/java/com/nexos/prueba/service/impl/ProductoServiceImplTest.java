package com.nexos.prueba.service.impl;

import com.nexos.prueba.entity.ProductoEntity;
import com.nexos.prueba.mapper.ProductoMapper;
import com.nexos.prueba.repository.ProductoRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.junit.jupiter.api.Assertions.assertNotNull;
import static org.mockito.ArgumentMatchers.anyLong;
import static org.mockito.ArgumentMatchers.anyString;
import static org.mockito.Mockito.when;


class ProductoServiceImplTest {
    @Mock
    private ProductoRepository productoRepository;

    @Mock
    private ProductoMapper productoMapper;

    @InjectMocks
    private ProductoServiceImpl productoServiceImpl;

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void findAll() {
        when(productoRepository.findAll()).thenReturn(getProductos());
        assertNotNull(productoServiceImpl.findAll());
    }

    @Test
    void findByFiltro() {
        when(productoRepository.findByFiltro(anyLong(), anyString())).thenReturn(getProductos());
        assertNotNull(productoServiceImpl.findByFiltro(new Long(0),""));
    }

    @Test
    void findById() {
        when(productoRepository.findById(anyLong())).thenReturn(getProducto());
        assertNotNull(productoServiceImpl.findByFiltro(new Long(0),""));
    }

    List<ProductoEntity> getProductos(){
        return new ArrayList<>();
    }

    Optional<ProductoEntity> getProducto(){
        return  Optional.of(new ProductoEntity());
    }

}