package com.nexos.prueba.configuration.exception;

public class NexosConflictResourceException extends Exception {

    public NexosConflictResourceException() { super(); }

    public NexosConflictResourceException(String message) {
        super(message);
    }

    public NexosConflictResourceException(String message, Throwable cause) {
        super(message, cause);
    }

    public NexosConflictResourceException(Throwable cause) {
        super(cause);
    }

    protected NexosConflictResourceException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}
