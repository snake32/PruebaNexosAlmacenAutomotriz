package com.nexos.prueba.dto;

import lombok.Data;

@Data
public class CargoDTO {

    private Long idCargo;
    private String nombre;

}