package com.nexos.prueba.configuration.error;

import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.Data;
import lombok.SneakyThrows;
import org.springframework.http.HttpStatus;

import java.util.Arrays;
import java.util.List;

@Data
public class NexosCustomError {

    private HttpStatus status;
    private int code;
    private String timestamp;
    private String message;
    private String type;
    private List<String> errors;
    private boolean hideModalError;

    public NexosCustomError(HttpStatus status, String message, List<String> errors, String timestamp, int code) {
        super();
        this.code = code;
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.errors = errors;
        this.hideModalError = false;
    }

    public NexosCustomError(HttpStatus status, String message, String error, String timestamp, int code) {
        super();
        this.code = code;
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList(error);
        this.hideModalError = false;
    }

    public NexosCustomError(HttpStatus status, String message, String error, String timestamp, int code, String type) {
        super();
        this.code = code;
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList(error);
        this.type = type;
        this.hideModalError = false;
    }

    public NexosCustomError(HttpStatus status, String message, String error, String timestamp, int code, boolean hideModalError) {
        super();
        this.code = code;
        this.timestamp = timestamp;
        this.status = status;
        this.message = message;
        this.errors = Arrays.asList(error);
        this.hideModalError = hideModalError;
    }

    @Override
    @SneakyThrows
    public String toString() { return (new ObjectMapper()).writeValueAsString(this); }
}