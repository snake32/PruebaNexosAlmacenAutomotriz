package com.nexos.prueba.dto;

import lombok.Data;

import java.util.Date;

@Data
public class UsuarioDTO {

    private Long idUsuario;
    private CargoDTO cargo;
    private String nombre;
    private int edad;
    private Date fechaIngresoCompañia;

}