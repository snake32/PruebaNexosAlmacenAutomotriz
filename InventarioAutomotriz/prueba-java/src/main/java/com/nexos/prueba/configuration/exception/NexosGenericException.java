package com.nexos.prueba.configuration.exception;

import lombok.Data;

@Data
public class NexosGenericException extends Exception {
    private String type;

    public NexosGenericException() {
        super();
    }

    public NexosGenericException(String message) {
        super(message);
    }

    public NexosGenericException(String message, String type) {
        super(message);
        this.type = type;
    }


    public NexosGenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public NexosGenericException(Throwable cause) {
        super(cause);
    }

    protected NexosGenericException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}