package com.nexos.prueba.util;

public class ConstantUtil {

    public static final String PRODUCTO_NOT_EXIST = "El producto no existe";
    public static final String EXISTING_USER = "El usuario es diferente al usuario de registro";
    public static final String QUANTITY_WHOLE_NUMBER = "La cantidad debe ser un número entero";
    public static final String DATE_ENTRY = "La fecha de ingreso debe ser menor o igual a la fecha Actual";
}
