package com.nexos.prueba.mapper;

import com.nexos.prueba.dto.UsuarioDTO;
import com.nexos.prueba.entity.UsuarioEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface UsuarioMapper {

    UsuarioDTO toDTO(final UsuarioEntity entity);
    UsuarioEntity toEntity(final UsuarioDTO dto);

}
