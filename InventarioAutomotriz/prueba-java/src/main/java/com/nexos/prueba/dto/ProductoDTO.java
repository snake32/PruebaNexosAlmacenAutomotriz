package com.nexos.prueba.dto;

import lombok.Data;

import java.util.Date;

@Data
public class ProductoDTO {

    private Long idProducto;
    private UsuarioDTO usuario;
    private String nombre;
    private int cantidad;
    private Date fechaIngreso;
    private String usuarioModificacion;

}