package com.nexos.prueba.service;

import com.nexos.prueba.dto.UsuarioDTO;

import java.util.List;

public interface UsuarioService {

    List<UsuarioDTO> findAll();

}