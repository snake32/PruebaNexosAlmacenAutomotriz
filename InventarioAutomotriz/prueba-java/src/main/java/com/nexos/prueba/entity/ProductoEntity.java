package com.nexos.prueba.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Data
@Entity
@Table(name = "producto")
public class ProductoEntity implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idProducto")
	private Long idProducto;
	
	@ManyToOne
	@JoinColumn(name = "idUsuario", nullable=false)
	private UsuarioEntity usuario;

	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "cantidad")
	private int cantidad;

	@Temporal(TemporalType.DATE)
	@Column(name = "fechaIngreso")
	private Date fechaIngreso;

	@Column(name = "usuarioModificacion")
	private String usuarioModificacion;

	@Temporal(TemporalType.DATE)
	@Column(name = "fechaModificacion")
	private Date fechaModificacion;

}