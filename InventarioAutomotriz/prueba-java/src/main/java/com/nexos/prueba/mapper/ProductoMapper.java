package com.nexos.prueba.mapper;

import com.nexos.prueba.dto.ProductoDTO;
import com.nexos.prueba.entity.ProductoEntity;
import org.mapstruct.Mapper;
import org.mapstruct.ReportingPolicy;

@Mapper(unmappedTargetPolicy = ReportingPolicy.IGNORE, componentModel = "spring")
public interface ProductoMapper {

    ProductoDTO toDTO(final ProductoEntity entity);
    ProductoEntity toEntity(final ProductoDTO dto);
}
