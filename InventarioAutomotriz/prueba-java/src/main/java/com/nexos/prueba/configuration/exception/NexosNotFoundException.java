package com.nexos.prueba.configuration.exception;

public class NexosNotFoundException extends Exception {
    public NexosNotFoundException() {
        super();
    }

    public NexosNotFoundException(String message) {
        super(message);
    }

    public NexosNotFoundException(String message, Throwable cause) {
        super(message, cause);
    }

    public NexosNotFoundException(Throwable cause) {
        super(cause);
    }

    protected NexosNotFoundException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}