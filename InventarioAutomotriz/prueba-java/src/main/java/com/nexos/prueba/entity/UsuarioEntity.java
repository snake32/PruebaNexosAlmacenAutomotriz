package com.nexos.prueba.entity;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.*;

import lombok.Data;

@Data
@Entity
@Table(name = "usuario")
public class UsuarioEntity implements Serializable {
	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	@Column(name = "idUsuario")
	private Long idUsuario;
	
	@ManyToOne
	@JoinColumn(name = "idCargo", nullable=false)
	private CargoEntity cargo;
	
	@Column(name = "nombre")
	private String nombre;
	
	@Column(name = "edad")
	private int edad;

	@Temporal(TemporalType.DATE)
	@Column(name = "fechaIngresoCompañia")
	private Date fechaIngresoCompañia;

}