package com.nexos.prueba.configuration.exception;

import lombok.Data;

@Data
public class NexosForbiddenException extends Exception{
    private String type;

    public NexosForbiddenException() {
        super();
    }

    public NexosForbiddenException(String message) {
        super(message);
    }

    public NexosForbiddenException(String message, Throwable cause) {
        super(message, cause);
    }

    public NexosForbiddenException(Throwable cause) {
        super(cause);
    }

    protected NexosForbiddenException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}