package com.nexos.prueba.configuration.error;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.exc.InvalidFormatException;
import com.nexos.prueba.configuration.exception.*;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindException;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.HttpRequestMethodNotSupportedException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

@Slf4j
@RestControllerAdvice
public class CustomRestExceptionHandler extends ResponseEntityExceptionHandler {

    private static final String DATE_FORMAT = "dd-MM-yyyy hh:mm:ss";

    private static final String GENERIC_ERROR_MESSAGE = "Generic Error Message";

    @Override
    protected ResponseEntity<Object> handleBindException(BindException ex, HttpHeaders headers,
                                                         HttpStatus status, WebRequest request) {
        NexosCustomError flashCustomError =
                new NexosCustomError(HttpStatus.BAD_REQUEST, "Los campos de la solicitud no son válidos",
                        getErrors(ex.getBindingResult().getFieldErrors(), ex.getBindingResult().getGlobalErrors()),
                        LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return handleExceptionInternal(ex, flashCustomError, headers, flashCustomError.getStatus(), request);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers,
                                                                  HttpStatus status, WebRequest request) {
        NexosCustomError flashCustomError =
                new NexosCustomError(HttpStatus.BAD_REQUEST, "Los campos de la solicitud no son válidos",
                        getErrors(ex.getBindingResult().getFieldErrors(), ex.getBindingResult().getGlobalErrors()),
                        LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return handleExceptionInternal(ex, flashCustomError, headers, flashCustomError.getStatus(), request);
    }

    private List<String> getErrors(List<FieldError> fieldErrors, List<ObjectError> objectErrors) {
        List<String> errors = new ArrayList<>();
        fieldErrors.forEach(fieldError -> errors.add(fieldError.getField() + ": " + fieldError.getDefaultMessage()));
        objectErrors.forEach(objectError -> errors.add(objectError.getObjectName() + ": " + objectError.getDefaultMessage()));
        return errors;
    }

    @Override
    protected ResponseEntity<Object> handleMissingServletRequestParameter(
            MissingServletRequestParameterException ex, HttpHeaders headers,
            HttpStatus status, WebRequest request) {
        String error = ex.getParameterName() + " falta el parámetro";

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return buildResponseEntity(flashCustomError);
    }

    @Override
    protected ResponseEntity<Object> handleHttpRequestMethodNotSupported(
        HttpRequestMethodNotSupportedException ex,
        HttpHeaders headers,
        HttpStatus status,
        WebRequest request) {

        StringBuilder builder = new StringBuilder();
        builder.append(ex.getMethod());
        builder.append(" El método no es compatible con esta solicitud. Los métodos admitidos son:: ");
        ex.getSupportedHttpMethods().forEach(t -> builder.append(t + " "));

        NexosCustomError flashCustomError = new NexosCustomError(HttpStatus.METHOD_NOT_ALLOWED,
            ex.getLocalizedMessage(), builder.toString(), LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());

        return buildResponseEntity(flashCustomError);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(final HttpMessageNotReadableException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {
        final Throwable cause = ex.getCause();

        if(cause instanceof InvalidFormatException){
            return this.handleInvalidFormatException((InvalidFormatException) cause, status, request);
        }else if(cause instanceof JsonParseException){
            return this.handleJsonParseException((JsonParseException) cause, status, request);
        }else if(cause instanceof JsonMappingException){
            return this.handleJsonMappingException((JsonMappingException) cause, status, request);
        }

        String error = "Uh oh, recibimos una solicitud vacía, verifique nuevamente";

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.BAD_REQUEST, "El RequestBody no está presente", error, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ MethodArgumentTypeMismatchException.class })
    public ResponseEntity<Object> handleMethodArgumentTypeMismatch(
            MethodArgumentTypeMismatchException ex, HttpStatus status, WebRequest request) {
        String error = ex.getName() + " debe ser de tipo " + ex.getRequiredType().getName();

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.BAD_REQUEST, ex.getLocalizedMessage(), error, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ InvalidFormatException.class })
    public ResponseEntity<Object> handleInvalidFormatException(
            InvalidFormatException ex, HttpStatus status, WebRequest request) {
        String error = "El valor '" + ex.getValue() + "' debe ser de tipo " + ex.getTargetType().getName();

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.BAD_REQUEST, GENERIC_ERROR_MESSAGE, error, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ JsonParseException.class })
    public ResponseEntity<Object> handleJsonParseException(
            JsonParseException ex, HttpStatus status, WebRequest request){

        String error = null;
        try {
            error = "Hubo un error cerca del token '" + ex.getProcessor().getCurrentName() + "' compruébelo por favor.";
        } catch (Exception e) {
            log.error("Exception", e);
        }

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.BAD_REQUEST, GENERIC_ERROR_MESSAGE, error, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ JsonMappingException.class })
    public ResponseEntity<Object> handleJsonMappingException(
            JsonMappingException ex, HttpStatus status, WebRequest request){

        String error = "Hubo un error cerca del token '" + ex.getPathReference().split("\"")[1] + "' compruébelo por favor.";

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.BAD_REQUEST, GENERIC_ERROR_MESSAGE, error, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), status.value());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ NexosConflictResourceException.class })
    public ResponseEntity<Object> handleFlashConflictResourceException(NexosConflictResourceException ex){

        NexosCustomError flashCustomError =
                new NexosCustomError(HttpStatus.CONFLICT, ex.getMessage(), GENERIC_ERROR_MESSAGE, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), HttpStatus.CONFLICT.value());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ NexosGenericException.class })
    public ResponseEntity<Object> handleFlashCustomException(NexosGenericException ex){

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.BAD_REQUEST, ex.getMessage(), GENERIC_ERROR_MESSAGE, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), HttpStatus.BAD_REQUEST.value(), ex.getType());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ NexosForbiddenException.class })
    public ResponseEntity<Object> handleFlashForbiddenException(NexosForbiddenException ex){

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.FORBIDDEN, ex.getMessage(), GENERIC_ERROR_MESSAGE, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), HttpStatus.FORBIDDEN.value(), ex.getType());
        return buildResponseEntity(flashCustomError);
    }


    @ExceptionHandler({NexosNotFoundException.class })
    public ResponseEntity<Object> handleFlashNotFoundException(NexosNotFoundException ex){

        NexosCustomError flashCustomError =
            new NexosCustomError(HttpStatus.NOT_FOUND, ex.getMessage(), GENERIC_ERROR_MESSAGE, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), HttpStatus.NOT_FOUND.value());
        return buildResponseEntity(flashCustomError);
    }

    @ExceptionHandler({ NexosNotGenericException.class })
    public ResponseEntity<Object> handleFlashCustomExceptionNone(NexosNotGenericException ex){

        NexosCustomError flashCustomError =
                new NexosCustomError(HttpStatus.BAD_REQUEST, ex.getMessage(), GENERIC_ERROR_MESSAGE, LocalDateTime.now().format(DateTimeFormatter.ofPattern(DATE_FORMAT)), HttpStatus.BAD_REQUEST.value(),true);
        return buildResponseEntity(flashCustomError);
    }

    private ResponseEntity<Object> buildResponseEntity(NexosCustomError flashCustomError) {
        log.error("Error status={} message={} ", flashCustomError.getStatus(), flashCustomError.getMessage());
        return new ResponseEntity<>(flashCustomError, flashCustomError.getStatus());
    }

}