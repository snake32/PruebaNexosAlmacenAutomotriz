package com.nexos.prueba.controller;

import java.util.List;

import com.nexos.prueba.configuration.exception.NexosGenericException;
import com.nexos.prueba.dto.ProductoDTO;
import com.nexos.prueba.service.ProductoService;
import com.nexos.prueba.configuration.exception.NexosNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/productos")
@CrossOrigin
public class ProductoController {

	@Autowired
	private ProductoService productoService;

	@GetMapping
	public List<ProductoDTO> findAll() {
		 return productoService.findAll();
	}

	@GetMapping("/filtro")
	public List<ProductoDTO> findByFiltro(@RequestParam Long usuario, @RequestParam String nombre) {
		return productoService.findByFiltro(usuario, nombre);
	}

	@GetMapping("/{idProducto}")
	public ProductoDTO findById( @PathVariable long idProducto) throws NexosNotFoundException {
		return productoService.findById(idProducto);
	}
	
	@PostMapping
	public ProductoDTO create(@RequestBody ProductoDTO producto) throws NexosGenericException {
		return productoService.create(producto);
	}

	@PutMapping("/{idProducto}")
	public ProductoDTO update(@RequestBody ProductoDTO productoDTO, @PathVariable long idProducto)
			throws NexosNotFoundException, NexosGenericException {
		return productoService.update(productoDTO, idProducto);
	}

	@DeleteMapping("/{idProducto}")
	public void delete(@PathVariable long idProducto, @RequestParam long idUsuario) throws NexosNotFoundException, NexosGenericException {
		productoService.delete(idProducto, idUsuario);
	}

}