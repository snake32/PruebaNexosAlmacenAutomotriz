package com.nexos.prueba.service;

import com.nexos.prueba.configuration.exception.NexosGenericException;
import com.nexos.prueba.dto.ProductoDTO;
import com.nexos.prueba.configuration.exception.NexosNotFoundException;

import java.util.List;

public interface ProductoService {

    List<ProductoDTO> findAll();
    List<ProductoDTO> findByFiltro(Long usuario,String nombre);
    ProductoDTO findById(long idProducto) throws NexosNotFoundException;
    ProductoDTO create (ProductoDTO productoDTO) throws NexosGenericException;
    ProductoDTO update (ProductoDTO productoDTO, long idProducto) throws NexosNotFoundException, NexosGenericException;
    void delete(long idProducto, long idUsuario) throws NexosNotFoundException, NexosGenericException;
}