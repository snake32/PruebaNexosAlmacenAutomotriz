package com.nexos.prueba.service.impl;

import com.nexos.prueba.dto.UsuarioDTO;
import com.nexos.prueba.mapper.UsuarioMapper;
import com.nexos.prueba.repository.UsuarioRepository;
import com.nexos.prueba.service.UsuarioService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class UsuarioServiceImpl implements UsuarioService {

    @Autowired
    UsuarioMapper mapper;

    @Autowired
    UsuarioRepository usuarioRepository;

    @Override
    public List<UsuarioDTO> findAll() {
        return usuarioRepository.findAll().stream().map(mapper::toDTO).collect(Collectors.toList());
    }

}
