package com.nexos.prueba.configuration.exception;

public class NexosNotGenericException extends Exception {

    public NexosNotGenericException() {
        super();
    }

    public NexosNotGenericException(String message) {
        super(message);
    }

    public NexosNotGenericException(String message, Throwable cause) {
        super(message, cause);
    }

    public NexosNotGenericException(Throwable cause) {
        super(cause);
    }

    protected NexosNotGenericException(String message, Throwable cause, boolean enableSuppression, boolean writableStackTrace) {
        super(message, cause, enableSuppression, writableStackTrace);
    }
}