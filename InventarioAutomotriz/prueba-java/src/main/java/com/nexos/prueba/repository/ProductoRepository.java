package com.nexos.prueba.repository;

import com.nexos.prueba.entity.ProductoEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ProductoRepository extends JpaRepository<ProductoEntity, Long>{

    ProductoEntity findByNombre(String nombre);

    @Query(value = "SELECT p.* FROM public.producto p INNER JOIN usuario u ON p.id_usuario=u.id_usuario WHERE UPPER(p.NOMBRE) LIKE %?2% AND (?1 = 0 OR u.id_usuario = ?1) ORDER BY id_producto ASC", nativeQuery = true)
    List<ProductoEntity> findByFiltro(Long usuario, String nombre);

}