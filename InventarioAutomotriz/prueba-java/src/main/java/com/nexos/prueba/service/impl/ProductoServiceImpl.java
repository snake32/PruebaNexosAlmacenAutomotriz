package com.nexos.prueba.service.impl;

import com.nexos.prueba.configuration.exception.NexosGenericException;
import com.nexos.prueba.service.ProductoService;
import com.nexos.prueba.dto.ProductoDTO;
import com.nexos.prueba.entity.ProductoEntity;
import com.nexos.prueba.configuration.exception.NexosNotFoundException;
import com.nexos.prueba.mapper.ProductoMapper;
import com.nexos.prueba.repository.ProductoRepository;
import com.nexos.prueba.util.ConstantUtil;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductoServiceImpl implements ProductoService {

    @Autowired
    ProductoMapper mapper;
    @Autowired
    ProductoRepository productoRepository;

    @Override
    public List<ProductoDTO> findAll() {
        return productoRepository.findAll().stream().map(mapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public List<ProductoDTO> findByFiltro(Long usuario, String nombre) {
        List<ProductoEntity> listProductoEntity = productoRepository.findByFiltro(usuario, nombre.toUpperCase());
        return listProductoEntity.stream().map(mapper::toDTO).collect(Collectors.toList());
    }

    @Override
    public ProductoDTO findById(long idProducto) throws NexosNotFoundException {

        ProductoEntity productoFound =  productoRepository.findById(idProducto)
                .orElseThrow(() -> new NexosNotFoundException(ConstantUtil.PRODUCTO_NOT_EXIST));

        return mapper.toDTO(productoFound);

    }

    @Override
    public ProductoDTO create(ProductoDTO productoDTO) throws NexosGenericException {
        ProductoEntity productoFound = productoRepository.findByNombre(productoDTO.getNombre());
        if(productoFound!=null) throw new NexosGenericException("El producto con el nombre de " + productoDTO.getNombre() + "ya existe");
        validation(productoDTO);
        ProductoEntity productoEntity = mapper.toEntity(productoDTO);
        productoEntity.setNombre(productoEntity.getNombre().toUpperCase());
        return mapper.toDTO(productoRepository.save(productoEntity));
    }

    @Override
    public ProductoDTO update(ProductoDTO productoDTO, long idProducto) throws NexosNotFoundException, NexosGenericException {

        ProductoEntity productoFound =  productoRepository.findById(idProducto)
                .orElseThrow(() -> new NexosNotFoundException(ConstantUtil.PRODUCTO_NOT_EXIST));

        ProductoEntity productoFoundByNombre = productoRepository.findByNombre(productoDTO.getNombre());

        if(productoFoundByNombre!=null && productoFoundByNombre.getIdProducto()!=idProducto)
            throw new NexosGenericException("El producto con el nombre de " + productoDTO.getNombre() + " ya existe");

        validation(productoDTO);

        productoFound.setNombre(productoDTO.getNombre().toUpperCase());
        productoFound.setCantidad(productoDTO.getCantidad());
        productoFound.setUsuarioModificacion(productoDTO.getUsuarioModificacion());
        productoFound.setFechaModificacion(new Date());

        return mapper.toDTO(productoRepository.save(productoFound));
    }

    @Override
    public void delete(long idProducto, long idUsuario) throws NexosNotFoundException, NexosGenericException {

        ProductoEntity productoFound =  productoRepository.findById(idProducto)
                .orElseThrow(() -> new NexosNotFoundException(ConstantUtil.PRODUCTO_NOT_EXIST));

        if(productoFound.getUsuario().getIdUsuario() != idUsuario) throw new
                NexosGenericException(ConstantUtil.EXISTING_USER);

        productoRepository.deleteById(idProducto);

    }

    private void validation(ProductoDTO productoDTO) throws NexosGenericException {

        if(productoDTO.getCantidad()<=0) throw new NexosGenericException(ConstantUtil.QUANTITY_WHOLE_NUMBER);

        Date date = new Date();

        if(productoDTO.getFechaIngreso().after(date)) throw new
                NexosGenericException(ConstantUtil.DATE_ENTRY);
    }

}