create database mercancia;

use mercancia;

INSERT INTO cargo (nombre) VALUES ('ASESOR DE VENTA');
INSERT INTO cargo (nombre) VALUES ('ADMINISTRADOR');
INSERT INTO cargo (nombre) VALUES ('SORPORTE');

INSERT INTO usuario (edad, fecha_ingreso_compañia, nombre, id_cargo) VALUES (50, '2020-02-03', 'Jose', '1');
INSERT INTO usuario (edad, fecha_ingreso_compañia, nombre, id_cargo) VALUES (35, '2019-05-08', 'Sebastian', '1');
INSERT INTO usuario (edad, fecha_ingreso_compañia, nombre, id_cargo) VALUES (55, '2010-05-06', 'Julia', '2');
INSERT INTO usuario (edad, fecha_ingreso_compañia, nombre, id_cargo) VALUES (26, '2018-09-07', 'Carlos', '3');

INSERT INTO producto (cantidad, fecha_ingreso, nombre, id_usuario) VALUES (50, '2022-08-09', 'PANTALON', '1');
INSERT INTO producto (cantidad, fecha_ingreso, nombre, id_usuario) VALUES (80, '2020-09-07', 'POLO', '2');
INSERT INTO producto (cantidad, fecha_ingreso, nombre, id_usuario) VALUES (90, '2022-07-08', 'SHORT', '3');
INSERT INTO producto (cantidad, fecha_ingreso, nombre, id_usuario) VALUES (100, '2021-06-01', 'MEDIAS', '4');
INSERT INTO producto (cantidad, fecha_ingreso, nombre, id_usuario) VALUES (200, '2021-07-05', 'SOMBRERO', '2');